go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip nomore403

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf nomore403 /opt/ANDRAX/bin/nomore403

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir -p /opt/ANDRAX/nomore403

cp -Rf payloads /opt/ANDRAX/nomore403

chown -R andrax:andrax /opt/ANDRAX/bin/nomore403
chmod -R 755 /opt/ANDRAX/bin/nomore403
